/* funkcja odposiada za ładowanie odpowiedniej podstrony WWW
	- name - zmienna, która odpowiada za nazwę przekazaną ze zeminnej-atrybutu przycisku HTML
	*/
function loadSite(name) {
	window.location = name + '.html';
}

/* funkcja sumująca 
	- a - parametr 1
	- b - parametr 2
	- zwraca sumę wartości a i b; 
	jeżeli chcemy, by sumowane były jedynie liczby należy dodać odpowiednie warunki
	takie jak np. isNaN() */
function sum(a, b) {
	return a + b;
}

/* funkcja dodająca zdarzenie do elementu/elementów reprezentowanych przez parametr s 
	- s - parametr mogący być nazwą elementu HTML, identyfikatorem bądź klasą 
	*/
function addEvents(s) {
	var btns = document.querySelectorAll(s); //funkcja wybierająca WSZYSTKIE elementy spełniające warunki/mające odpowiednią nazwę/klasę
	for (var i = 0; i < btns.length; i++) { 
	/* każdy wybrany element musi mieć oddzielenie indywiduwalnie nadaną funkcje nasłuchu 
		poniżej funkcja zakłada, że nadajemy obbsługę na kliknięcie (click); celem 
		nadania większej uniwersalności funkcji można przekazywać nazwę operacji zdarzenia
		jako dodatkowy parametr)
		*/
		btns[i].addEventListener('click', function(e) {
			/* ponieważ nadanie wartości zmiennej utworzononej w ramach wykonywanej funkcji
			   niszczy kod (zmienna żyje tylko do jej zakończenia, później występuje jako undefined)
			   poniższy kod pobiera interesującą nas wartość bezpośrednio z danych elementu*/
			var node = e.target; //najpierw pobieramy element HTML, na którym zostało wywołane zdarzenie (reprezentowane przez zmienną e)
			//poniższa pętla sprawdza czy atrybut data posiada zmienną name
			//jeżeli nie (będzie występować jako undefined) pobierany jest kolejnuy rodzic (element nadrzędny)
			//obecnie wybranego elementu HTML
			while (node.dataset.name === undefined)
				node = node.parentNode;
			loadSite(node.dataset.name); //cel zdarzenia -> załadowanie odpowiedniej strony WWW
		});
	}
}

/* ====================================================================================================
 * Poniższe funkcje dotyczą odczytu oraz obsługi danych pobranych z pliku JSON
 * ====================================================================================================
 */

 /* funkcja obrabia odebrane dane z pliku JSON oraz wybiera tylko te zapisane pozycje, które zawierają 
    poszukiwaną frazę 
	- jsonDATA - pełna zawartość przekaznego pliku JSON (bez przetworzenia)
	- p - fraza zapytania
*/
/* DO ZROBIENIA!!
	- rozbiecie fraz ze zeminnej p na tablicę (użytkownik może podać kilka słów-kluczy)
	- stworzenie zmiennej zliczającej wystąpienie słów-kluczy w danym wierszu
	- wychwytywanie pozostałych słów-kluczy dla w danym wierszu 
	- jeżeli słów będzie więcej niż 3 odnośnik do pliku powinien być pogrubiony!
	- dodać wszystkie niezbędne opcje CSS (oraz odpowiednie klasy przy elementach wyników)
	- dodać obsługę błędu na wypadek gdyby jsonDATA nie posiadał zawartości (bądź będzie typu undefined)
	- opcjonalnie można dodać pozycjonowanie wyników (więcej słów-kluczy -> wyższa pozycja) - OPCJA NIEOBOWIĄZKOWA!!
*/
function generateSearchText(jsonDATA, p) {
	//funkcja odpowiada za utworzenie obiektu JavaScript z danych pliku JSON
	//ze względu na budowę pliku (tablica z elementem głównym - właściwym obiektem JSON)
	//po utworzeniu obiektu od razu wybieramy tenże element (wskazanie na pierwszy element tablicy)
	//ponieważ pierwszy element JSON również jest tablicą (zawierającą potencjalnie kolejne wiersze danych)
	//wybieramy go poprzez wskazanie jego nazwy (wiersze)
	//w przypadku JSON można odwoływać się zarówno w formacie właściwości (jak poniżej)
	//jak również w postaci tablicowej, np. new Function('return ' + jsonDATA)()[0]["wiersze"]
	var dataRows = new Function('return ' + jsonDATA)()[0].wiersze;
	//powyższą funkcje można również zrealizować poprzez specjalny obiekt JavaScript JSON
	//niektóre starsze przeglądarki mogą mieć jednak z nią problem (aczkolwiek stanowią obecnie niewielki procent rynku)
	// ------------------------------------------------------------------------------------------------------
	//var dataRows = JSON.parse(jsonDATA)[0].wiersze; <- OPISYWANA FUNKCJA
	//-------------------------------------------------------------------------------------------------------
	//ostatnią możliwością (najmniej polecaną) jest użycie funkcji eval(). Jest ona jedną z najstarszych funkcji JavaScript
	//i potrafi bez problemu przetwarzać DOWOLNE typy danych na inne. Niestety to podowduje, iż jest niezwykle niebezpieczna
	//(może przykładowo posłużyć jako wektor ataku na naszą witrynę i/lub komputery naszych użytkowników)
	//wspomniana jest jedyne w celu poznawczym, NIE NALEŻY Z NIEJ KORZYSTAĆ!
	//-------------------------------------------------------------------------------------------------------
	//var dataRows = eval(jsonDATA)[0].wiersze; <- OPISYWANA FUNKCJA
	//-------------------------------------------------------------------------------------------------------
	var search = false; //ustalamy zmienną, która wskaże, czy w danym wierszu danych odnalezione zostało słowo kluczowe
	/* poniżej tworzona jest zmienna-uchwyt do elementu będącego pierwszym dzieckiem elementu-rodzina z identyfikatorem (id)
	   o nazwie 'contentLeft'. Elementem tym jest znacznik article (na stronie z pliku szukaj.html nie zawiera ŻADNEJ TREŚCI)
	   Będzie on uzupełniany o odpowiednie treści
	*/
	var mainElement = document.getElementById('contentLeft').children[0];  
	for (var i = 0; i < dataRows.length; i++) { //pętla przechodząca przez wszystkie elementy-wiersze z danymi pliku JSON
		for (var j = 0; j < dataRows[i]["klucze"].length; j++) { //klucz o nazwie 'klucze' jest tablicą zawierającą szereg danych, stąd pętla
			if (p === dataRows[i]["klucze"][j]) { //jeżeli wartość klucza zgadza się wartością podaną pod zmienną p
				search = true; //zmieniamy wartość zmiennej search na true (prawda)
				break; //przerywamy pierwszą pętlę for! (zawsze słowo break przerywa pierwszą pętlę, w której zostanie wywołane)
			}
		}	
		//poniższy warunek wykona się jedynie w przypadku gdy wartość search będzie prawdą (true)
		if (search) {
			var searchRow = document.createElement('div'); //dynamicznie tworzymy nowy element div
			searchRow.className += " searchResult"; //dodajemy do niego nazwę klasy searchResult
			var searchTitle = document.createElement('p'); //tworzymy element p (paragraf)
			var searchAddress = document.createElement('a'); // tworzymy element odnośnika (kotwicę)
			searchAddress.href = dataRows[i]["adres"]; //atrybut odnośnika ustawiamy na wartość zapisaną pod polem adres 
			searchAddress.innerHTML = dataRows[i]["tytul"]; //zawartość znacznika (pomiędzy <a> oraz </a>) uzupełniamy zawartością pola tytul
			searchTitle.appendChild(searchAddress); //cały odnośnik dodajemy jako dziecko do tworzonego elementu paragrafu
			var searchDescription = document.createElement('p'); //tworzymy kolejny element paragrafu
			searchDescription.innerHTML = dataRows[i]["opis"]; //dodajemy do niego zawartość znajdującą się pod kluczem opis
			searchRow.appendChild(searchTitle); //dodajemy paragraf z odnośnikiem do naszej utworzonej warstwy
			searchRow.appendChild(searchDescription); //dodajemy opis do warstwy
			mainElement.appendChild(searchRow); //dodajemy warstwę do wcześniej przechwyconego pola article (będącego już na stronie
		}
	}
}

/* Funkcja do zastosowania przy przycisku Wyszukaj. Pozwala na pobranie zawartości z pliku JSON
   oraz przekazanie jego zawartości do funkcji obrabaiającej pozyskane dane 
   - phrases - zmienna przechowująca frazy, które mają zostać wyszukane w bazie
   - funkcja zwraca wrtość 0 w przypadku powodzenia bądź tekst błędu w przypadku wystąpienia problemu
   */ 
/* DO ZROBIENIA!!!!
	- dodać obsługę błędów zarówno tranferu plików jak i kodów HTTP (401, 403, 500, 502 itp.)
*/
function getSearchData(phrases) {
	var remoteFile = new XMLHttpRequest(); //utworzenie obiektu odpowiedzialnego za asynchroniczne przesyłanie danych
	
	/* poniższa operacja NIE WYKONA się sekwencyjnie!!!
	Poniższa linia kodu ma za zadanie przypisać operację do zdarzenia ONREADYSTATECHANGFE.
	Zdarzenie to wykonywane jest przez JavaScript samoistnie - gdy tylko zmieni się stan 
	utworzonego przez nas obiektu. Na jego stan wpływa zaś komunikacja klient-serwer.
	W chwili pojawienia zdarzenia (zmiana stanu) chcemy by wykonała się seria rozkazów. Można podać nazwę istniejącej funkcji
	jednak tworzenie nazwy funkcji by wykonała się dokładnie raz w konkretnej sytuacji w języku JavaScript nie jest
	najlepszym rozwiązaniem. Dlatego tworzymy tutaj funkcję anonimową (bez nazwy) nie przyjmującą żadnych parametrów.
	Funkcja ta będzie miała za zadanie obsługę przyjętych przez JavaScript danych ze wskazanego pliku zdalnego (z serwera) */
	remoteFile.onreadystatechange = function() { //w nowszych wersjach JavaScript można zmienić tę deklarację na poniższą:
	//remoteFile.onreadystatechange = () => { //niektóre wersje przeglądarek mogą mieć problem z tego typu deklaracją i potraktować ją jako błąd
		if (remoteFile.readyState === 4) { //kod o numerze 4 oznacza iż transfer zawartości zdalnej się zakończył poprawnie
			/* 
				status 200 to nic innego jak kod HTTP iż dokument został poprawnie rozpoznany i przesłany do klienta
				(przeglądarki). Status 0 odbierany jest w większości przeglądarek firmy Microsoft (jednak tych starszych)
			*/
			if (remoteFile.status === 200 || remoteFile.status === 0) {
				generateSearchText(remoteFile.responseText, phrases); //wywołanie funkcji, która zajmie się obróbką i wyświetleniem danych
				return 0; //zwrócenie kodu 0 -> w naszym przypadku wszystko wykonało się poprawnie
			}
			else if (remoteFile.status === 404) //kod 404 oznacza brak dokumentu na serwerze (lokalizacja odległa). 
				return "Strona o podanym adresie nie istnieje";
			else //każdy inny błąd
				return "Błąd serwera HTTP o numerze: " + remoteFile.status;
		}
		//poniższy fragment obsługuje każdy możliwy błąd transferu danych, będzie jednak niezrozumiały dla 
		//większości użytkowników (wyświetli się tylko numer/kod błędu)
		else {
			return "Plik nie mógł zostać wczytany ze względu na błąd (kod błędu " + remoteFile.readyState + ")";
		}
	}
	/* KONIEC KODU obsługi zdarzenia; WSZYSTKIE POZOSTAŁE LINIE wykonają się sekwencyjnie (czyli po sobie) i czasie jedna po drugiej*/
	
	/* funkcja odpowiedzialna za otwarcie połączenia z serwerem
		- pierwszy parametr określa w jaki sposób ma przebiegać komunikacja do serwera (przesłanie danych do serwera). 
		Ponieważ nie przesyłamy danych sposób jest dowolny (wybrany został POST; dostępne jest kilka innych opcji)
		- drugi parametr określa dokument, do którego odwołujemy się na serwerze. Ścieżka może być względna (tak jak ta poniżej)
		bądź bezwzględna (np. http://mojsuperadres.pl/plik.txt). Rozszerzenie pliku nie ma znaczenia 
		- ostatni parametr oznacza tryb pracy połączenia. Wartość true oznacza połączenie asynchroniczne (zdarzenie)
		Wartość flase wskazywałaby na połączenie blokowe (synchroniczne), tj. sekwencyjne. OBECNIE PRZESTARZAŁE/USUWANE!! (nie należy używać) */
	remoteFile.open("POST", "./json/searchbase.json", true); 
	//poniższa linia jest opcjonalna; dodatkowo upewniamy się iż zawartość przesyłana pomiędzy serwerem i klientem będzie zakodowana;
	//nie chodzi jednak o szyfrowanie a możliwość przesyłania danych innych niż tekstowe (np. pliki binarne, wykonywalne itp.)
	//może być przydatna w późniejszych implementacjach (obecnie przesyłany jest jedynie tekst)
	remoteFile.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//dodatkowy, opcjonalny nagłówek HTTP dzięki któremu serwer będzie wiedział iż akceptujemy tylko pliki o typie json bądź javascript
	//generalnie nagłówek ten powinien być automatycznie wystawiany przez połączenie (można tę linię pominąć)
	remoteFile.setRequestHeader('Accept', 'application/json, text/javascript');
	remoteFile.send(); //funkcja, która powoduje wysłanie danych żądania do serwera; w przypadku 
	//pracy asynchronicznej kończy działanie naszej funkcji!!
}

